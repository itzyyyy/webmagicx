# webmagicx

### webmagicx一款基于webmagic的可配置化的爬虫框架


webmagicx是一款可配置爬虫框架，webmagicx中的webmagic表示该框架扩展于webmagic，x表示该框架是一个基于xml的配置型爬虫框架。
得益于webmagic强大的可扩展能力，本框架实现了以下特性：


- 无需写任何代码，只需你熟悉正则表达式和xpath，通过简单的配置便可实现一个爬虫。
- 实现了基于corn的定时调度功能。
- 提供了文本替换、正则查找、拆分等处理逻辑
- 提供了文件下载功能。
- 提供了简单通用存储功能，能够将抓取的数据轻松存入数据库和文件。
- 在webmagic的基础上实现了深度抓取。
- 基于rest的监控
- 多线程支持。

### 要求

jdk1.8以上


### 快速开始

 **安装 webmagicx** 

webmagicx 提供了二进制安装包
[webmagicx 下载页面](https://gitee.com/luosl/webmagicx/releases)，你可以在这个页面下载 webmagicx 的最新版本。

下载完成后，将webmagicx-xx.zip解压，你会得到以下目录结构:

```
webmagicx
 spiderConf                      → 这里存放了一些爬虫配置的模板文件
   douban.spider.xml             → 抓取豆瓣电影的示例
   baike_yixue.spider.xml        → 抓取百度百科疾病诊断信息的示例
   template.spider.xml           → 配置模板
 bin                             → 存放命令脚本的文件夹                          
   webmagicx-cli.bat             → windos 客户端命令  
   webmagicx-cli.sh              → linux 客户端命令 
   webmagicx-server.bat          → windos 服务端命令 
   webmagicx-server.sh           → linux 服务端命令 
 conf                            → 项目配置
   log4j.properties              → 项目日志配置文件      
 lib                             → 项目依赖 jar 包
   ....                              
```
 **执行第一个爬虫程序** 

进入webmagicx的bin目录

在 windows 环境下，你可以按住Shift键+鼠标右键 选择 "在此处打开命令" 打开windows命令行。输入命令: 
```
webmagicx-server.bat -confPath ../spiderConf/douban.spider.xml
```

在linux 环境下 首先需要为 .sh 文件赋予执行权限：
```
chmod a+x ./*.sh
```
然后执行命令:
```
./webmagicx-server.sh -confPath ../spiderConf/douban.spider.xml
```

待爬虫运行一段时间后，在bin目录中会生成   **_豆瓣电影.csv_**  文件和  **_img_**   文件夹，分别存放了电影信息和电影封面，如下图所示：

![电影信息](https://gitee.com/uploads/images/2018/0228/171023_847badb2_142580.png "QQ截图20180228170942.png")

![电影封面](https://gitee.com/uploads/images/2018/0228/171042_c02ff187_142580.png "QQ截图20180228170955.png")


打开浏览器 输入网址 http://localhost:9000/spider/state/douban 便可查看爬虫的运行状态:

![爬虫运行状态](https://gitee.com/uploads/images/2018/0301/172456_5e042692_142580.png "QQ截图20180301172422.png")
### 项目文档
[项目文档](https://gitee.com/luosl/webmagicx/wikis/%E7%AE%80%E4%BB%8B)

### 说明

项目才刚刚开始，大家有什么建议和想法欢迎一起交流。同时也希望有兴趣和精力的盆友一起来完善这个项目 :smile: 

### QQ群

468248192