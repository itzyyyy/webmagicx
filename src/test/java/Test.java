import java.math.BigInteger;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created by luosl on 2018/3/13.
 */
public class Test {

    public static class PrimeProducer extends Thread {


        private BlockingQueue<BigInteger> queue = new ArrayBlockingQueue<BigInteger>(10);
        private boolean cancle = false;

        @Override
        public void run() {
            try {
                BigInteger i = BigInteger.ONE;
                while(!Thread.currentThread().isInterrupted()){
                    queue.put(i.nextProbablePrime());
                }
            }catch (InterruptedException e){
                System.out.println("中断 阻塞");
            }
        }

        public void cancle(){
            interrupt();
            System.out.println("cancel");
        }
    }

    public static void main(String[] args) throws Exception {

        PrimeProducer producer = new PrimeProducer();
        producer.start();
        TimeUnit.SECONDS.sleep(3);
        producer.cancle();
    }
}
