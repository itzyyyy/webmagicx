#!/usr/bin/env bash
export JAVA_HOME=$JAVA_HOME
PRG="$0"
while [ -h "$PRG" ]; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`/"$link"
    fi
    done
PRGDIR=`dirname "$PRG"`
[ -z "$WEBMAGICX_HOME" ] && WEBMAGICX_HOME=`cd "$PRGDIR/.." >/dev/null; pwd`
CONF_DIR="$WEBMAGICX_HOME/conf"
LIB_DIR="$WEBMAGICX_HOME/lib"
SPIDER_CONF_DIR="$WEBMAGICX_HOME/spiderConf"
CLASS_PATH=$CONF_DIR
for i in `ls $LIB_DIR/*.jar`
do
  CLASS_PATH=${CLASS_PATH}:${i}
done
$JAVA_HOME/bin/java -classpath "$CLASS_PATH" "org.luosl.webmagicx.Client" $1 $2 $3 $4 $5 $6 $7 $8 $9
