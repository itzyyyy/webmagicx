package org.luosl.webmagicx.conf

class ConfException(msg:String, t:Throwable = new Throwable) extends RuntimeException(msg, t){

}

object ConfException{
  def apply(msg: String, t: Throwable): ConfException = new ConfException(msg, t)
  def apply(msg: String): ConfException = new ConfException(msg)
}
