package org.luosl.webmagicx.conf

/**
  * 默认参数配置
  * Created by luosl on 2017/12/4.
  */
object DefaultConfValue {
  val enable:Boolean = true

  val maxDeep:Int = -1
  val charset:String = "UTF-8"
  val timeout:Int = 20000
  val threadNum:Int = 1
  val retryTimes:Int = 3
  val sleep:Int = 1000

  val scope:String = "head"
  val extractType:String = "xpath"
  val textFormat:Boolean = false
  val must:Boolean = true
}
