package org.luosl.webmagicx.listeners

import us.codecraft.webmagic.{ResultItems, Task}

/**
  * Created by luosl on 2017/12/15.
  */
trait PipelineListener {

  def onSuccess(resultItems: ResultItems, task:Task)

  def onError(resultItems: ResultItems, task:Task)

  def onSkip(resultItems: ResultItems, task:Task)

}
