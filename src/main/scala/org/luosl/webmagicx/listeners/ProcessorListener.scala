package org.luosl.webmagicx.listeners

import org.luosl.webmagicx.conf.Field
import us.codecraft.webmagic.{Page, ResultItems, Task}

/**
  * Created by luosl on 2017/12/15.
  */
trait ProcessorListener {

  def onSuccess(page: Page)

  def onError(page: Page)

  def onSkip(page: Page, field:Field)

}
