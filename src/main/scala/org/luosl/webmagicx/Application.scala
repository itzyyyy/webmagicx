package org.luosl.webmagicx

import org.luosl.webmagicx.utils.Logging
import org.luosl.webmagicx.cli.Cli
import org.luosl.webmagicx.conf._
import org.luosl.webmagicx.monitor.HttpSpiderMonitor

/**
  * Created by luosl on 2017/11/6.
  */
class Application(cli:Cli) extends Logging{

  /** 监控管理 */
  private val monitor:HttpSpiderMonitor = new HttpSpiderMonitor(
    cli.optionValOrDefault("host", "0.0.0.0"),
    cli.optionValOrDefault("port", "9000").toInt
  )

  /**
    * 启动服务
    */
  def startService(): Unit ={
    try {
      monitor.start()
      val confPathOpt:Option[String] = cli.optionValOpt("confPath")
      if(confPathOpt.isDefined){
        val scs:Array[SpiderConf] = ConfLoader.listAllConf(confPathOpt.get)
        logInfo(s"载配置文件加载完毕!!")
        scs.foldLeft(1){ (count,conf)=>
          logInfo(s"正在提交爬虫任务[desc=${conf.desc}],当前进度:$count / ${scs.length}")
          val spiderCell:SpiderCell = SpiderCell(conf)
          monitor.submitSpiderTask(spiderCell)
          count + 1
        }
      }
    }catch {
      case e:Exception =>
        e.printStackTrace()
        monitor.shutdown()
    }
  }

}

object Application {

  def main(args: Array[String]): Unit = {
    val cli:Cli = Cli(args:_*)
    new Application(cli).startService()
  }

}
