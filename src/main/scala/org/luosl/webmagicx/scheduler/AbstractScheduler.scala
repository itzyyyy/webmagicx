package org.luosl.webmagicx.scheduler

import java.util.Comparator

import org.luosl.webmagicx.conf.{SpiderConf, XmlProps}
import org.luosl.webmagicx.utils.Logging
import us.codecraft.webmagic.{Request, Task}
import us.codecraft.webmagic.scheduler.{DuplicateRemovedScheduler, MonitorableScheduler}

/**
  * Created by luosl on 2018/3/15.
  */
abstract class AbstractScheduler(sc:SpiderConf, task:Task, props:XmlProps) extends DuplicateRemovedScheduler with MonitorableScheduler with Logging{

  /**
    * 优先级比较函数
    */
  protected val requestComparator:Comparator[Request] = (r1:Request, r2:Request) => - r1.getPriority.compareTo(r2.getPriority)

  /**
    * 所有的优先级列表
    */
  protected val allPriority:Seq[Long] = (0L :: sc.targetUrlRegexs.map(_.priority).toList).distinct.sorted.reverse

}
