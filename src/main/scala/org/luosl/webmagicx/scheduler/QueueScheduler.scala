package org.luosl.webmagicx.scheduler

import java.io.Closeable

import org.luosl.webmagicx.conf.{SpiderConf, XmlProps}
import us.codecraft.webmagic.Task
import us.codecraft.webmagic.scheduler.PriorityScheduler
import us.codecraft.webmagic.scheduler.component.HashSetDuplicateRemover

/**
  * Created by luosl on 2017/12/6.
  */
@Deprecated
class QueueScheduler(sc:SpiderConf, task:Task, props:XmlProps) extends us.codecraft.webmagic.scheduler.QueueScheduler with Closeable{
  override def close(): Unit = {
    this.setDuplicateRemover(new HashSetDuplicateRemover)
  }
}
