package org.luosl.webmagicx.processor

import java.io.Closeable

import org.luosl.webmagicx.conf.{Field, SpiderConf}
import org.luosl.webmagicx.utils.Logging
import org.luosl.webmagicx.listeners.ProcessorListener
import us.codecraft.webmagic.{Page, Site}
import us.codecraft.webmagic.processor.PageProcessor
import us.codecraft.webmagic.utils.UrlUtils

import scala.collection.mutable.ArrayBuffer

/**
  * Created by luosl on 2017/11/7.
  */
abstract class AbstractProcessor(sc:SpiderConf) extends PageProcessor with Logging with Closeable{

  /**
    * 监听器
    */
  protected var listeners:ArrayBuffer[ProcessorListener] = ArrayBuffer.empty[ProcessorListener]

  private val site:Site = {
    val sites:Site = Site.me()
    sites.setCharset(sc.attribute.charset) // 设置字符集
    sites.setTimeOut(sc.attribute.timeout) // 设置超时
    sites.setCycleRetryTimes(sc.attribute.retryTimes) // 设置重试次数
    sites.setUserAgent(sc.site.userAgent) // 设置浏览器标识
    sites.setSleepTime(sc.attribute.sleep) // 设置抓取间隔
    sc.site.headers.foreach(kv=>sites.addHeader(kv._1,kv._2)) // 设置header
    sc.site.cookies.foreach(kv=>sites.addCookie(kv._1,kv._2)) // 设置 cookie
    sites
  }

  /**
    * 添加一个监听器
    * @param listener listener
    * @return
    */
  def addListener(listener:ProcessorListener):AbstractProcessor = {
    listeners += listener
    this
  }

  def onSkip(page:Page, f:Field): Unit ={
    listeners.foreach(ls=>ls.onSkip(page, f))
  }

  def onError(page:Page): Unit ={
    listeners.foreach(ls=>ls.onError(page))
  }

  def onSuccess(page:Page): Unit ={
    listeners.foreach(ls=>ls.onSuccess(page))
  }

  override def getSite: Site = site

  override def close(): Unit ={

  }

}
